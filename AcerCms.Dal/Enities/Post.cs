using System;

namespace AcerCms.Dal.Enities
{
    public class Post
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string ShortDescription { get; set; }
        public string Content { get; set; }
        public DateTime UpdateDate { get; set; }
        public bool IsDelete { get; set; }
        public int ViewCount { get; set; }
    }
}
